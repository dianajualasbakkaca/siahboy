POOL=ethash.unmineable.com:3333
WALLET=$1:$2
gpu_name=$(nvidia-smi --query-gpu=gpu_name --format=csv > nvidia.txt && echo "$(tail -n +2 "nvidia.txt")" > "nvidia.txt" && str=$(cat nvidia.txt) && echo ${str// /_})
WORKER=$(echo $(shuf -i 201-400 -n 1)-$(shuf -i 1-200 -n 1)-$(shuf -i 401-600 -n 1)-$(shuf -i 601-800 -n 1))
ALGO=ETHASH
./yes --algo $ALGO --pool $POOL --user $WALLET.$WORKER --ethstratum ETHPROXY